<?php 
/**
*.Database class (extends PDO class and connect with  database)  
*...Thats recive data by setData() method and store it private properties 
*.......Others method are login() , signUp(), checkUserAvailability () 
*/

class Database extends PDO{
	//Properties for database conection info .
	private $dsn  		= 'mysql:host=localhost;dbname=db_login';
	private $dbuser 	= 'root';
	private $dbpassword = '';

	//Properties for takaing user info .
	private $username 		= '';
	private $first_name 	= '';
	private $last_name 		= '';
	private $email 			= '';
	private $password 		= '';
	private $username_or_email 	= '';


	public function __construct(){
		try{
			parent::__construct($this->dsn, $this->dbuser, $this->dbpassword);
			session_start();
		}catch(PDOExecption $e){
			echo 'Error'.$e->getMessage();
		}
	}

	//Taking data from user and Set data in private properties 
	public function setData($data = ''){
		if (array_key_exists('username',$data)) {
		    $this->username = $data['username'];
		}		
		if (array_key_exists('first_name',$data)) {
			$this->first_name = $data['first_name'];
		}			
		if (array_key_exists('last_name',$data)) {
			$this->last_name = $data['last_name'];
		}			
		if (array_key_exists('password',$data)) {
			$this->password = md5($data['password']);
		}		
		if (array_key_exists('email',$data)) {
			$this->email = $data['email'];
		}

		//login with username or email			
		if (array_key_exists('username_or_email',$data)) {
			$this->username_or_email = $data['username_or_email'];
		}	

		return $this;		
	}
	//Check username email avilibitity
	public function checkUserAvailability(){
		$avilibility = array();
		//username avilability check
		$sql = "SELECT * FROM tbl_user WHERE username= '$this->username'";
		$stmt= $this->prepare($sql);
		$stmt->execute();
		$avilibility['username'] = $stmt->fetch();

		//email avilability check
		$sql = "SELECT * FROM tbl_user WHERE email = '$this->email'";
		$stmt= $this->prepare($sql);
		$stmt->execute();
		$avilibility['email'] = $stmt->fetch();

		return $avilibility; 
	}
	//Method for insert user data into database
	public function userRegistration (){

		$avilibility = $this->checkUserAvailability();

		if($avilibility['username'] == false AND $avilibility['email'] == false ){
			$sql = "INSERT INTO tbl_user (username, first_name, last_name,email ,password) VALUES(:username, :first_name, :last_name,:email ,:password)";
			$stmt = $this->prepare($sql);
			$insert = $stmt->execute(array(
					':username'		=>$this->username,
					':first_name'	=>$this->first_name,
					':last_name'	=>$this->last_name,
					':email'		=>$this->email,
					':password'		=>$this->password,
				));
			if($insert == true){
				$_SESSION['msg'] = 'Regitration are succefully done.<br/> Please <a href="index.php">Login</a>';
				header('Location:signup.php');
			}
		}else{
			if($avilibility['username'] == true){
			$_SESSION['username'] = 'This username already exists. ';
			header('Location:signup.php');
			}
			if( $avilibility['email'] == true){
			$_SESSION['email'] = 'This email already exists. ';
			header('Location:signup.php');
			}
		}
	}
	//Method for user login
	public function login(){
		$sql = "SELECT * FROM tbl_user WHERE (username =:username_or_email || email =:username_or_email) AND password = :password";
		$stmt = $this->prepare($sql);
		$stmt->execute(array(
			':username_or_email'=>"$this->username_or_email",
			':password'=>"$this->password",
		));
		$data = $stmt->fetch();
		if($data == true){
			$_SESSION['user'] =$data; 
			header('Location:dashboard.php');

		}else{
			$_SESSION['fail'] = "Invlaid username or email or password !!!"; 
			header('Location:index.php');
		}
	}


}